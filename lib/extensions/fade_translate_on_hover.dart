import 'package:flutter/material.dart';

class FadeTranslateOnHover extends StatefulWidget {
  final Widget child;
  // You can also pass the translation in here if you want to
  FadeTranslateOnHover({Key key, this.child}) : super(key: key);

  @override
  _FadeTranslateOnHoverState createState() => _FadeTranslateOnHoverState();
}

class _FadeTranslateOnHoverState extends State<FadeTranslateOnHover> {
  final nonHoverTransform = Matrix4.identity()..translate(0, 0, 0);
  final hoverTransform = Matrix4.identity()..translate(0, -10, 0);

  bool _hovering = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (e) => _mouseEnter(true),
      onExit: (e) => _mouseEnter(false),
        child: AnimatedContainer(
          duration: const Duration(milliseconds: 200),
          child: widget.child,
          color: !_hovering ? Colors.transparent : Colors.white,
          transform: _hovering ? hoverTransform : nonHoverTransform,
        ),
      
    );
  }

  void _mouseEnter(bool hover) {
    setState(() {
      _hovering = hover;
    });
  }
}