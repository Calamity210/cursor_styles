library pointer_styles;

import 'package:cursor_styles/extensions/fade_translate_on_hover.dart';
import 'package:cursor_styles/extensions/translate_on_hover.dart';
import 'package:flutter/material.dart';
import 'dart:html' as html;

extension HoverExtensions on Widget {

  static final appContainer =
      html.window.document.getElementById('app-container');

      setAsAuto() {
    appContainer.style.cursor = 'auto';
  }

  Widget get showCursorOnHover {
    return MouseRegion(
      child: this,
      // When the mouse enters the widget set the cursor to pointer
      onHover: (event) {
        appContainer.style.cursor = 'pointer';
      },
      // When it exits set it back to default
      onExit: (event) {
        appContainer.style.cursor = 'default';
      },
    );
  }

  Widget get showTextOnHover {
    return MouseRegion(
      child: this,
      // When the mouse enters the widget set the cursor 
      onHover: (event) {
        appContainer.style.cursor = 'text';
      },
      // When it exits set it back to default
      onExit: (event) {
        appContainer.style.cursor = 'default';
      },
    );
  }

   Widget get showCopyOnHover {
    return MouseRegion(
      child: this,
      // When the mouse enters the widget set the cursor 
      onHover: (event) {
        appContainer.style.cursor = 'copy';
      },
      // When it exits set it back to default
      onExit: (event) {
        appContainer.style.cursor = 'default';
      },
    );
  }

  Widget get showHelpOnHover {
    return MouseRegion(
      child: this,
      // When the mouse enters the widget set the cursor 
      onHover: (event) {
        appContainer.style.cursor = 'help';
      },
      // When it exits set it back to default
      onExit: (event) {
        appContainer.style.cursor = 'default';
      },
    );
  }

    Widget get showZoomInOnHover {
    return MouseRegion(
      child: this,
      // When the mouse enters the widget set the cursor 
      onHover: (event) {
        appContainer.style.cursor = 'zoom-in';
      },
      // When it exits set it back to default
      onExit: (event) {
        appContainer.style.cursor = 'default';
      },
    );
  }

  Widget get showZoomOutOnHover {
    return MouseRegion(
      child: this,
      // When the mouse enters the widget set the cursor 
      onHover: (event) {
        appContainer.style.cursor = 'zoom-out';
      },
      // When it exits set it back to default
      onExit: (event) {
        appContainer.style.cursor = 'default';
      },
    );
  }

  Widget get showCrosshairOnHover {
    return MouseRegion(
      child: this,
      // When the mouse enters the widget set the cursor 
      onHover: (event) {
        appContainer.style.cursor = 'crosshair';
      },
      // When it exits set it back to default
      onExit: (event) {
        appContainer.style.cursor = 'default';
      },
    );
  }

  Widget get showWaitOnHover {
    return MouseRegion(
      child: this,
      // When the mouse enters the widget set the cursor 
      onHover: (event) {
        appContainer.style.cursor = 'wait';
      },
      // When it exits set it back to default
      onExit: (event) {
        appContainer.style.cursor = 'default';
      },
    );
  }

   

  Widget get moveUpOnHover {
    return TranslateOnHover(
      child: this,
    );
  }

  Widget get fadeInOnHover {
    return FadeTranslateOnHover(
      child: this,
    );
  }

}
